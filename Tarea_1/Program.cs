﻿using System;

namespace Tarea_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean jugar = true;
            while (jugar == true)
            {
                Proceso_Minas x = new Proceso_Minas();
                int tamaño = 0;
                int minas = 0;
                int X = 10;
                int Y = 12;
                //
                x.marco(80, 2, 110, 20, 1);
                x.inicio();
                Console.SetCursorPosition(83, 6);
                Console.WriteLine("DIGITE SU NOMBRE");
                Console.SetCursorPosition(83, 7);
                String nom = Console.ReadLine();
                Console.SetCursorPosition(83, 8);


                Console.SetCursorPosition(101, 9);
                //
                String[,] mat = new string[tamaño, tamaño];
                int difi = int.Parse(Console.ReadLine());
                if (difi == 1)
                {
                    tamaño = 8;
                    minas = 12;
                    mat = x.MatrizBomba(tamaño, minas);
                    x.ImprimeMatriz(tamaño, mat);
                    x.marco(X, X, Y, Y, tamaño);
                    x.juego(11, 11, tamaño, mat);


                }

                else if (difi == 2)
                {
                    tamaño = 10;
                    minas = 20;
                    mat = x.MatrizBomba(tamaño, minas);
                    x.ImprimeMatriz(tamaño, mat);
                    x.marco(X, X, Y, Y, tamaño);
                    x.juego(11, 11, tamaño, mat);
                }
                else if (difi == 3)
                {
                    tamaño = 12;
                    minas = 30;
                    mat = x.MatrizBomba(tamaño, minas);
                    x.ImprimeMatriz(tamaño, mat);
                    x.marco(X, X, Y, Y, tamaño);
                    x.juego(11, 11, tamaño, mat);
                }
                else
                {
                    Console.WriteLine("Opcion no existe");
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("DESEA CONTINUAR\n 1.Si \n 2. NO ");
                int opc = Convert.ToInt32(Console.ReadLine());
                if (opc == 1)
                {
                    jugar = true;
                }
                else
                    jugar = false;
                x.limpiar(0, 0, 115, 50);
                Console.ForegroundColor = ConsoleColor.White;
            }

            Console.ForegroundColor = ConsoleColor.White;
        } //METODO
    }//CLASE
}
