﻿/*=================================================================================================
Institute....: Universidad Técnica Nacional
Headquarters.: Pacífico
Career.......: Tecnologías de la Información
Period.......: 1-2020
Solution.....: HOMEWORK 1
Goals........: learn to develop a mine game.
Professor....: Jorge Ruiz (york)
Student......: Adonay Acevedo Poveda
               Angelica Vegas Nuñez
               KENDALL MAURICIO CASTRO ROSALES
=================================================================================================*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Tarea_1
{
    class Proceso_Minas
    {
        int puntos = 0;
        public void inicio()
        {
            Console.SetCursorPosition(83, 4);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("BIENVENIDO AL BUSCA MINAS");
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(83, 9);
            Console.Write("Digite su opción [ ]");
            Console.SetCursorPosition(83, 11);
            Console.WriteLine("Selecione una dificultad");
            Console.SetCursorPosition(83, 13);
            Console.WriteLine("1-Principiante");
            Console.SetCursorPosition(83, 15);
            Console.WriteLine("2-Intermedio");
            Console.SetCursorPosition(83, 17);
            Console.WriteLine("3-Experto");
        }
        public void limpiar(int col1, int fil1, int col2, int fil2)
        {
            for (int fl = fil1; fl <= fil2; fl++)
            {
                for (int cl = col1; cl <= col2; cl++)
                {
                    Console.SetCursorPosition(cl, fl);
                    Console.Write(" ");
                }
            }
        }

        public void ImprimeMatriz(int tamaño, String[,] mat)
        {
            int num = 25;
            for (int i = 0; i < tamaño; i++)
            {
                Console.SetCursorPosition(80, num);
                for (int j = 0; j < tamaño; j++)
                {

                    Console.Write(mat[i, j]);
                }

                num++;
                Console.WriteLine();
            }
        }

        public String[,] MatrizBomba(int tam, int minas)
        {
            String[,] mat = new string[tam, tam];
            for (int i = 0; i < tam; i++)
            {
                for (int b = 0; b < tam; b++)
                {
                    mat[i, b] = "o";
                }
            }
            Random x = new Random();
            int conta = 0;
            do
            {
                int A = x.Next(tam - 1);
                int B = x.Next(tam - 1);
                if (A == 0 & B == 0)
                {
                    mat[A, B] = "o";

                }
                else
                {
                    mat[A, B] = "*";
                    conta++;
                }
            } while (conta < minas);


            return mat;
        }//METODO

        public void marco(int col1, int fil1, int col2, int fil2, int tam)
        {
            for (int x = 0; x < tam; x++)
            {
                for (int y = 0; y < tam; y++)
                {
                    for (int fl = fil1; fl <= fil2; fl++)
                    {
                        for (int cl = col1; cl <= col2; cl++)
                        {
                            if ((fl == fil1) || (fl == fil2))
                            {
                                Console.SetCursorPosition(cl, fl); Console.Write("─");
                            }
                            else if (((cl == col1) || (cl == col2)) && ((fl > fil1) || (fl < fil2)))
                            {
                                Console.SetCursorPosition(cl, fl); Console.Write("│");
                            }

                            if ((fl == fil1) && (cl == col1))
                            {
                                Console.SetCursorPosition(cl, fl); Console.Write("┌");
                            }
                            else if ((fl == fil1) && (cl == col2))
                            {
                                Console.SetCursorPosition(cl, fl); Console.Write("┐");
                            }

                            if ((fl == fil2) && (cl == col1))
                            {
                                Console.SetCursorPosition(cl, fl); Console.Write("└");
                            }
                            else if ((fl == fil2) && (cl == col2))
                            {
                                Console.SetCursorPosition(cl, fl); Console.Write("┘");
                            }
                        }// End column control
                    }// End row control
                    col1 += 3;
                    col2 += 3;
                    if (y == tam - 1)
                    {
                        col1 = 10;
                        col2 = 12;
                        fil1 += 3;
                        fil2 += 3;
                    }
                }
                // End marco function
            }//for
        }

        public void juego(int X, int Y, int lim, String[,] mat)
        {
            char opc = ' ';
            int I = 0;
            int J = 0;
            int tam = lim;
            Boolean Game_Over = true;

            if (lim == 8)
            {
                lim = 32;
            }
            else
            if (lim == 10)
            {
                lim = 38;
            }
            else
            if (lim == 12)
            {
                lim = 42;
            }
            else
            { }
            while (Game_Over)
            {
                Console.SetCursorPosition(X, Y);
                Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine("█");
                Console.SetCursorPosition(X, Y);
                switch (opc = Console.ReadKey().KeyChar)
                {
                    case 'a':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (X > 11)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            X -= 3;
                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");
                        }
                        if (J > 0)
                        {
                            J--;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;

                            }



                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                    case 'd':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (X < lim)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            X += 3;
                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");
                        }
                        if (J < tam - 1)
                        {
                            J++;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;
                            }
                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                    case 'q':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (X > 11 & Y > 11)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            X -= 3;
                            Y -= 3;
                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");
                        }
                        if (I > 0 & J > 0)
                        {
                            I--;
                            J--;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;

                            }



                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                    case 'w':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (Y > 11)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            Y -= 3;
                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");
                        }
                        if (I > 0)
                        {
                            I--;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;

                            }



                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                    case 's':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (Y < lim)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            Y += 3;
                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");
                        }
                        if (I < tam - 1)
                        {
                            I++;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;

                            }


                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                    case 'e':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (Y > 11 & X < lim)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            Y -= 3;
                            X += 3;
                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");
                        }
                        if (I > 0 & J < tam - 1)
                        {
                            I--;
                            J++;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;

                            }



                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                    case 'z':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (Y < lim & X > 11)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            Y += 3;
                            X -= 3;

                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");


                        }
                        if (I < tam - 1 & J > 0)
                        {
                            I++;
                            J--;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;
                            }

                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                    case 'c':
                        if (mat[I, J] == "o")
                        {
                            puntos = puntos + 100;
                            mat[I, J] = "#";
                        }
                        else
                        {
                            puntos = puntos - 15;
                        }
                        if (X < lim & Y < lim)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                            X += 3;
                            Y += 3;
                            Console.SetCursorPosition(X, Y);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("█");
                        }
                        if (I < tam - 1 & J < tam - 1)
                        {
                            I++;
                            J++;
                            Console.SetCursorPosition(58, 5);
                            Console.WriteLine("MINAS CERCANAS = " + MinasCercanas(I, J, mat, tam));
                            if (mat[I, J] == "*")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a Perdido");
                                Console.SetCursorPosition(83, 41);
                                Console.WriteLine("Sus puntos son:0");
                                Game_Over = false;

                            }



                            if (I == tam - 1 && J == tam - 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                                Console.SetCursorPosition(X, Y); Console.WriteLine("█");
                                Console.SetCursorPosition(83, 40);
                                Console.WriteLine("Usted a ganado Sus puntos son:{0}", puntos);
                                Game_Over = false;

                            }
                        }
                        break;
                }

            }



        }//END GAME
        //Start Minas

        public int MinasCercanas(int X, int Y, String[,] mat, int tam)
        {
            int conta = 0;
            if (Y < tam - 1)
            {
                if (mat[X, Y + 1] == "*")//D
                {
                    conta++;
                }
            }
            if (Y > 0)
            {
                if (mat[X, Y - 1] == "*")//A
                {
                    conta++;
                }
            }
            if (X > 0)
            {
                if (mat[X - 1, Y] == "*")//W
                {
                    conta++;
                }
            }
            if (X < tam - 1)
            {
                if (mat[X + 1, Y] == "*")//S
                {
                    conta++;
                }
            }
            if (X < tam - 1 & Y < tam - 1)
            {
                if (mat[X + 1, Y + 1] == "*")//C
                {
                    conta++;
                }
            }
            if (X < tam - 1 & Y > 0)
            {
                if (mat[X + 1, Y - 1] == "*")//Z
                {
                    conta++;
                }
            }
            if (X > 0 & Y < tam - 1)
            {
                if (mat[X - 1, Y + 1] == "*")//E
                {
                    conta++;
                }
            }
            if (X > 0 & Y > 0)
            {
                if (mat[X - 1, Y - 1] == "*")//Q
                {
                    conta++;
                }
            }
            return conta;
        }

    }
}
